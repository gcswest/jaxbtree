#!/bin/bash

cd $(dirname $0)/../../../target/generated-sources/jaxb/com/harmonic/electra/skel/generated
for i in *.java
do
tr -d '\r' < $i |
tr -s '\n' >tmp &&
mv tmp $i
done

