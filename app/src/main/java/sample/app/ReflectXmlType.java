package sample.app;


import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.*;
import com.google.gwt.editor.rebind.model.ModelUtils;
import com.google.gwt.user.rebind.ClassSourceFileComposerFactory;
import com.google.gwt.user.rebind.SourceWriter;
import com.harmonic.electra.skel.generated.CSDChassis;
import sample.app.client.ClassPair;
import sample.app.client.IntroSpectXmlType;

import javax.xml.bind.annotation.XmlType;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ReflectXmlType extends Generator {
//  private static final String IMPL_TYPE_NAME =
//    IntroSpectXmlType.class.getSimpleName() + "Impl";
//  private static final String IMPL_PACKAGE_NAME =
//    IntroSpectXmlType.class.getPackage().getName();

    public static String printString(ClassPair p) {
        String f = MessageFormat.format("\n\tnew ClassPair({0}.class,", p.claz.getCanonicalName());
        String s;
        if (p.map == null || p.map.isEmpty()) s = "null";
        else {
            s = " new LinkedHashMap() {{";
            for (Map.Entry<String, ClassPair> stringClassPairEntry : p.map.entrySet
                    ()) {
                s += MessageFormat.format("\n\t\tput(\"{0}\",{1}); ",
                        stringClassPairEntry.getKey(), printString(stringClassPairEntry.getValue()));
            }
            s +=
                    "}}\n";
        }
        f += s + ")\n";
        return f;
    }

    @Override
    public String generate(TreeLogger treeLogger, GeneratorContext context,
                           String requestedClass) throws UnableToCompleteException {
        treeLogger.log(TreeLogger.Type.INFO, "entering genreator for " + requestedClass);
        TypeOracle typeOracle = context.getTypeOracle();
        JClassType classType = typeOracle.findType(requestedClass);
        final String pkgName = classType.getPackage().getName();
        String simpleSourceName = classType.getSimpleSourceName();
//                           new ClassSourceFileComposerFactory( pkgName, simpleSourceName + "Impl");

        ClassSourceFileComposerFactory composerFactory = new ClassSourceFileComposerFactory(pkgName, simpleSourceName + "Impl");
        composerFactory.addImplementedInterface(requestedClass);
        composerFactory.addImport(LinkedHashMap.class.getCanonicalName());
        composerFactory.addImport(ClassPair.class.getCanonicalName());
        composerFactory.setJavaDocCommentForClass("generated to provide a static model for tree rendering from JAXB proxies");


        PrintWriter printWriter = context.tryCreate(treeLogger, pkgName, simpleSourceName + "Impl");

        if (printWriter!=null) {
            SourceWriter sourceWriter = null;
            try {

                final JClassType[] parameterizationOf = ModelUtils.findParameterizationOf
                        (typeOracle.getType(IntroSpectXmlType.class.getCanonicalName()),
                                typeOracle.findType(requestedClass));

                final JClassType jClassType = parameterizationOf[0];

                final String qualifiedSourceName = jClassType.getQualifiedSourceName();

    //      System.err.println(qualifiedSourceName);
                final ClassPair xmlTypes = getXmlTypes(Class.forName(qualifiedSourceName));

                treeLogger.log(TreeLogger.Type.INFO, "creating printwriter for " + qualifiedSourceName);
                String xmlTypeDump = printString(xmlTypes);
                treeLogger.log(TreeLogger.Type.TRACE, "inserting data: " + xmlTypeDump);
                sourceWriter = composerFactory.createSourceWriter(context, printWriter);
                sourceWriter.println("public ClassPair getXmlTypes(){");
                sourceWriter.println("return " + xmlTypeDump + ";");
                sourceWriter.println("};");
            } catch (NotFoundException e) {
                e.printStackTrace();  //todo: verify for a purpose
            } catch (NoSuchFieldException e) {
                e.printStackTrace();  //todo: verify for a purpose
            } catch (ClassNotFoundException e) {
                e.printStackTrace();  //todo: verify for a purpose
            }
            assert sourceWriter != null;
            sourceWriter.commit(treeLogger);
         }
        final String createdClassName = composerFactory.getCreatedClassName();
        return createdClassName;
    }


    @SuppressWarnings({"NonJREEmulationClassesInClientCode"})
    static public void main(String... args) throws NoSuchFieldException {

        printXmlTypes(CSDChassis.class);
        System.err.println(printString(getXmlTypes(CSDChassis.class)));
    }

    @SuppressWarnings({"NonJREEmulationClassesInClientCode"})
    static public ClassPair getXmlTypes(Class<?> aClass) throws
            NoSuchFieldException {
        final LinkedHashMap<String, ClassPair> ret = new LinkedHashMap<String, ClassPair>();
        if (aClass.isAnnotationPresent(XmlType.class)) {
            final XmlType annotation = aClass.getAnnotation(XmlType.class);
            final String[] orderedProps = annotation.propOrder();
            for (String prop : orderedProps) {
                Class<?> childClass = aClass.getDeclaredField(prop).getType();
                ret.put(prop, getXmlTypes(childClass));
            }
        }
        return new ClassPair(aClass, ret);
    }

    @SuppressWarnings({"NonJREEmulationClassesInClientCode"})
    public static void printXmlTypes(Class<?> aClass) throws NoSuchFieldException {
        if (!aClass.isAnnotationPresent(XmlType.class)) return;
        System.err.println("for: " + aClass.toString());
        final XmlType annotation = aClass.getAnnotation(XmlType.class);
        final String[] orderedProps = annotation.propOrder();
        for (String prop : orderedProps) {
            final Field declaredField = aClass.getDeclaredField(prop);
            Class<?> childClass = declaredField.getType();
            if (Collection.class.isAssignableFrom(childClass)) {
                final Type genericType = declaredField.getGenericType();
                Class lp = (Class<?>) ((java.lang.reflect.ParameterizedType) genericType).getActualTypeArguments()[0];
                if (!lp.isPrimitive() && !lp.isArray() && !String.class.isAssignableFrom(lp)) {
                    System.err.println("*--: " + prop + " : " + lp.toString());
                    printXmlTypes(lp);
                }
            } else if (childClass.isPrimitive() || childClass.isArray()) {
                System.err.println(" +:" + prop + " : " + childClass);
            } else printXmlTypes(childClass);
        }
    }
}
