package sample.app;


import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.core.ext.typeinfo.JMethod;
import com.google.gwt.core.ext.typeinfo.NotFoundException;
import com.google.gwt.core.ext.typeinfo.TypeOracle;
import com.google.gwt.editor.rebind.model.ModelUtils;
import com.google.gwt.query.client.builders.XmlBuilder;
import com.google.gwt.user.rebind.ClassSourceFileComposerFactory;
import com.google.gwt.user.rebind.SourceWriter;
import sample.app.client.ClassPair;

import javax.xml.bind.annotation.XmlType;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.LinkedHashMap;

public class BuilderTypeGenerator extends Generator {

    @Override
    public String generate(TreeLogger treeLogger, GeneratorContext context,
                           String requestedClass) throws UnableToCompleteException {
        treeLogger.log(TreeLogger.Type.INFO, "entering generator for " + requestedClass);
        TypeOracle typeOracle = context.getTypeOracle();
        JClassType classType = typeOracle.findType(requestedClass);
        final String pkgName = classType.getPackage().getName();
        String simpleSourceName = classType.getSimpleSourceName();

        ClassSourceFileComposerFactory composerFactory = new ClassSourceFileComposerFactory(pkgName, simpleSourceName /*+ "Impl"*/);

        PrintWriter printWriter = context.tryCreate(treeLogger, pkgName, simpleSourceName /*+ "Impl"*/);

        if (printWriter != null) {
            SourceWriter sourceWriter = null;
            try {

                final JClassType[] parameterizationOf = ModelUtils.findParameterizationOf(typeOracle.getType(sample.app.client.BuilderType.class.getCanonicalName()), typeOracle.findType(requestedClass));
                final JClassType jClassType = parameterizationOf[0];

                final String qualifiedSourceName = jClassType.getQualifiedSourceName();

                //      System.err.println(qualifiedSourceName);

                final ClassPair xmlTypes = getXmlTypes(Class.forName(qualifiedSourceName));

                treeLogger.log(TreeLogger.Type.INFO, "creating printwriter for " + qualifiedSourceName);

                sourceWriter = composerFactory.createSourceWriter(context, printWriter);
//                sourceWriter.println("public ClassPair getBuilder(){");
//                sourceWriter.println("return " + xmlTypeDump + ";");
//                sourceWriter.println("};");

                sourceWriter.println("public interface "+ requestedClass + " extends "+ XmlBuilder.class.getCanonicalName()+"{");
                JMethod[] methods = jClassType.getMethods();
                for (JMethod method : methods) {
                    String readableDeclaration = method.getReadableDeclaration();
                    sourceWriter.println(readableDeclaration + ";");
                }
                sourceWriter.println("};");

            } catch (NotFoundException e) {
                e.printStackTrace();  //todo: verify for a purpose
            } catch (NoSuchFieldException e) {
                e.printStackTrace();  //todo: verify for a purpose
            } catch (ClassNotFoundException e) {
                e.printStackTrace();  //todo: verify for a purpose
            }
            assert sourceWriter != null;
            sourceWriter.commit(treeLogger);
        }
        return composerFactory.getCreatedClassName();
    }


//    @SuppressWarnings({"NonJREEmulationClassesInClientCode"})
//    static public void main(String... args) throws NoSuchFieldException {
//
//        printXmlTypes(CSDChassis.class);
//        System.err.println(printString(getBuilder(CSDChassis.class)));
//    }

    @SuppressWarnings({"NonJREEmulationClassesInClientCode"})
    static public ClassPair getXmlTypes(Class<?> aClass) throws
            NoSuchFieldException {
        final LinkedHashMap<String, ClassPair> ret = new LinkedHashMap<String, ClassPair>();
        if (aClass.isAnnotationPresent(XmlType.class)) {
            final XmlType annotation = aClass.getAnnotation(XmlType.class);
            final String[] orderedProps = annotation.propOrder();
            for (String prop : orderedProps) {
                Class<?> childClass = aClass.getDeclaredField(prop).getType();
                ret.put(prop, getXmlTypes(childClass));
            }
        }
        return new ClassPair(aClass, ret);
    }

    @SuppressWarnings({"NonJREEmulationClassesInClientCode"})
    public static void printXmlTypes(Class<?> aClass) throws NoSuchFieldException {
        if (!aClass.isAnnotationPresent(XmlType.class)) return;
        System.err.println("for: " + aClass.toString());
        final XmlType annotation = aClass.getAnnotation(XmlType.class);
        final String[] orderedProps = annotation.propOrder();
        for (String prop : orderedProps) {
            final Field declaredField = aClass.getDeclaredField(prop);
            Class<?> childClass = declaredField.getType();
            if (Collection.class.isAssignableFrom(childClass)) {
                final Type genericType = declaredField.getGenericType();
                Class lp = (Class<?>) ((java.lang.reflect.ParameterizedType) genericType).getActualTypeArguments()[0];
                if (!lp.isPrimitive() && !lp.isArray() && !String.class.isAssignableFrom(lp)) {
                    System.err.println("*--: " + prop + " : " + lp.toString());
                    printXmlTypes(lp);
                }
            } else if (childClass.isPrimitive() || childClass.isArray()) {
                System.err.println(" +:" + prop + " : " + childClass);
            } else printXmlTypes(childClass);
        }
    }
}
