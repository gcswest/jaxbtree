package sample.app.client;

import java.util.Map;

public class ClassPair {


    public final Class claz;

    public final Map<String, ClassPair> map;

    boolean isList = false;


    public ClassPair(Class claz, Map<String, ClassPair> map) {
        this.claz = claz;
        this.map = map;
    }
}
