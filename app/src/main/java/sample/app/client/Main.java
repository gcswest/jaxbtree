package sample.app.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;

/**
 * Created by IntelliJ IDEA.
 * User: jim
 * Date: 9/23/12
 * Time: 8:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class Main extends Composite {

  interface MainUiBinder extends UiBinder<FlowPanel, Main> {
  }

  private static MainUiBinder ourUiBinder = GWT.create(MainUiBinder.class);

  private FlowPanel rootWidget;

  {
    rootWidget = ourUiBinder.createAndBindUi(this);
    initWidget(rootWidget);
  }
}
