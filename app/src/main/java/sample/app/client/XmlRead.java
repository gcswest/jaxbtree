package sample.app.client;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.query.client.Function;
import com.google.gwt.query.client.GQuery;
import com.harmonic.electra.skel.generated.CSDChassis;

import static com.google.gwt.user.client.Window.alert;


public class XmlRead implements EntryPoint {
    interface ChassisBuilder extends BuilderType<CSDChassis> {
    }

    @Override
    public void onModuleLoad() {
        ChassisBuilder cb= GWT.create(ChassisBuilder.class);


        GQuery.get("test.json", null, new Function() {
            public void f() {
                // Create the Site instance
                ChassisBuilder s = GWT.create(ChassisBuilder.class);
                // Load the data got from the server
                s.load(getDataProperties());
                // We can use standard getters and setters,
                // making the code more readable and type-safe
                alert("OK " +
                        s.toString());
            }
        });

    }
}
