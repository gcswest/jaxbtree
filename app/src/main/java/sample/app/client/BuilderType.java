package sample.app.client;

import com.google.gwt.query.client.builders.XmlBuilder;

public interface BuilderType<T> extends XmlBuilder {
    ClassPair getBuilder();
}
